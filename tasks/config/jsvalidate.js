module.exports = function(grunt) {
  grunt.config.set('jsvalidate', {
    dev: {
      files: [require('../pipeline').jsFilesToInject]
    }
  });
  grunt.loadNpmTasks('grunt-jsvalidate');
};

