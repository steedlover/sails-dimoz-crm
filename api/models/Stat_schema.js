module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: 'string',
      unique: true,
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    toJSON: function() {
      return this.toObject();
    }
  },
  getStatObj: function(cb) {
    this.find().exec(function(err, statsArr) {
      if(err) {
        sails.log.error(err)
        return res.redirect(500);
      }
      var result = {};
      statsArr.forEach(function(item) {
        result[item.id] = item;
      });
      cb(result);
      return;
    });
  }
};

