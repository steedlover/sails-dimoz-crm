module.exports = {
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'string'
    },
    importance: {
      type: 'integer',
      required: true,
      autoIncrement: true
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  },
  getRoleByID: function(ID, _attr, callback) {
    this.findOne(ID).exec(function(err, obj) {
      if(err) {
        sails.log.error(err);
        return res.view('500');
      }
      var result = !_attr ? obj : eval('obj.' + _attr);
      if(callback && typeof(callback) == 'function') return callback(err, result);
      else return result;
    });
  },
  getRolesObj: function(curRoleImportance, callback) {
    var result = {};
    result.obj = {};
    this.find().exec(function(err, roles) {
      if(err) {
        sails.log.error(err);
        return res.view('500');
      }
      roles.forEach(function(item) {
        if(curRoleImportance < item.importance) {
          eval('result.obj[' + parseInt(item.id) + '] = {' + 'name: "' + item.name + '", ' + 'importance: ' + parseInt(item.importance) + '}');
        }
      });
      var selectDefStr = '';
      var rolesArr = Object.keys(result.obj);
      /*** String to integer values of arr ***/
      var intArr = [];
      rolesArr.forEach(function(key) {
        intArr.push(parseInt(key));
      });
      /*** ******************************* ***/
      rolesArr = intArr;
      rolesArr.forEach(function(key, index) {
        selectDefStr += '{role: ' + parseInt(key) + '}';
        selectDefStr += rolesArr.length != index + 1 ? ',' : '';
      });
      result.arr = rolesArr;
      /*** Where definition for user selection ***/
      result.where = {
        or: eval('[' + selectDefStr + ']')
      };
      if(callback && typeof(callback) == 'function') return callback(err, result);
      else return result;
    });
  },
  getAvailableRoles: function(req, res, callback) {
    this.find().where({
      'importance': {
        '>': parseInt(req.session.User.roleImportance)
      }
    }).sort('importance').exec(function(err, roles) {
      if(err) {
        sails.log.error(err);
        //return res.redirect('500');
      }
      return callback(err, roles);
    });
  },
  analyzeOrder: function(newIDArr, curObjArr, callback) {
    var diffArr = [];
    newIDArr.forEach(function(ID, index) {
      if(curObjArr[index].id != ID) diffArr.push({
        'id': ID,
        'importance': index + 2
      });
    });
    return callback(diffArr);
  }
};

