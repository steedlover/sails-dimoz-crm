module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    action_id: {
      type: 'integer',
      required: true,
      index: true,
      model: 'actions'
    },
    stat: {
      type: 'integer',
      required: true
    },
    stat_schema: {
      model: 'stat_schema',
      columnName: 'stat'
    },
    datetime: {
      type: 'datetime',
      defaultsTo: function() {
        return new Date();
      }
    },
    toJSON: function() {
      return this.toObject();
    }
  }
};

