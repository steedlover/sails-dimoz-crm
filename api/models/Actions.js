module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    owner: {
      model: 'user'
    },
    archive: {
      type: 'boolean',
      defaultsTo: false
    },
    type: {
      type: 'string',
      required: true
    },
    fullname: {
      type: 'string',
      required: true
    },
    phone: {
      type: 'string',
      required: true
    },
    mailbox: {
      type: 'email',
      required: true
    },
    log: {
      collection: 'actions_log',
      via: 'action_id'
    },
    visit_params: {
      type: 'string'
    },
    add_fields: {
      type: 'string'
    },
    toJSON: function() {
      return this.toObject();
    }
  }
};

