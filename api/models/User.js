module.exports = {
  //autoPk: false,
  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true
    },
    login: {
      type: 'string',
      unique: true,
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    mail: {
      type: 'string',
      unique: true,
      required: true,
      email: true
    },
    password: {
      type: 'string',
      required: true
    },
    role: {
      type: 'integer',
      required: true
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      delete obj._csrf;
      return obj;
    }
  },
  beforeCreate: function(values, next) {
    if(!values.password || !values.confirm || values.password != values.confirm) return next("Passwords doesn't match");
    this.cryptPassword(values.password, function(err, encryptedPassword) {
      if(err) return next(err);
      values.password = encryptedPassword;
      return next();
    });
  },
  cryptPassword: function(password, next) {
    var salt = 10;
    require('bcrypt').hash(password, salt, function(err, encryptedPassword) {
      return next(err, encryptedPassword);
    });
  },
  getSessionUser: function(req) {
    return req.session.authenticated && req.session.User && typeof(req.session.User) == 'object' ? req.session.User : null;
  },
  getAllUsers: function(req, next) {
    Roles.getRolesObj(req.session.User.roleImportance, function(err, roles) {
      User.count().where(roles.where).exec(function(err, count) {
        User.find().where(roles.where).sort('createdAt DESC').exec(function(err, list) {
          next(list);
          return;
        });
      });
    });
  }
};

