module.exports = {
  index: function(req, res) {
    res.view({
      layout: sails.config.views.layout,
      controller: '',
      title: LocalizBySess.__('Register page', req),
      sessionUser: User.getSessionUser(req),
      menuPoints: sails.config.blueprints.publicControllers
    });
  }
};

