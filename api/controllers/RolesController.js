module.exports = {
  _config: {},
  getRolesList: function(req, res) {
    Roles.getAvailableRoles(req, res, function(err, list) {
      if(err) {
        sails.log.error(err);
        res.view('500');
      }
      var obj = {};
      if(list.length > 0) {
        list.forEach(function(role) {
          obj[role.id] = role.name;
        });
      }
      return res.json(obj);
    });
  },
  table: function(req, res) {
    Roles.getAvailableRoles(req, res, function(err, list) {
      if(err) {
        sails.log.error(err);
        res.view('500');
      }
      sails.renderView('roles/table', {
        list: list,
        layout: '../' + sails.config.views.ajaxLayout
      }, function(err, html) {
        res.json(JSON.stringify(html));
      });
    });
  },
  select: function(req, res) {
    var selectedID = req.param('selected') ? req.param('selected') : null;
    Roles.getAvailableRoles(req, res, function(err, list) {
      if(err) {
        sails.log.error(err);
        res.view('500');
      }
      sails.renderView('roles/select', {
        selected: selectedID,
        list: list,
        layout: '../' + sails.config.views.ajaxLayout
      }, function(err, html) {
        var responseObj = {
          count: list.length,
          text: html
        };
        res.json(JSON.stringify(responseObj));
      });
    });
  },
  saveImportance: function(req, res) {
    var newOrderArr = JSON.parse(req.param('data'));
    Roles.getAvailableRoles(req, res, function(err, curRolesArr) {
      if(err) sails.log.error(err);
      Roles.analyzeOrder(newOrderArr, curRolesArr, function(diffArr) {
        var diffNum = diffArr.length;
        var saved = 0;
        diffArr.forEach(function(newVarsObj, index) {
          Roles.update(newVarsObj.id, {
            importance: newVarsObj.importance
          }).then(function() {
            saved++;
            if(saved == diffNum) res.json('done');
          });
        });
      });
    });
  }
}

