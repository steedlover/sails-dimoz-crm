module.exports = {
  index: function(req, res) {
    res.view({
      layout: sails.config.views.adminLayout,
      controller: '',
      title: 'Start page',
      sessionUser: User.getSessionUser(req),
      menuPoints: sails.config.blueprints.publicControllers
    });
  }
};

