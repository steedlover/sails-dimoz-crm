module.exports = {
  _config: {
    controller: 'user'
  },
  getAllRows: function(req, res) {
    User.getAllUsers(req, function(list) {
      return res.json(list);
    });
  },
  getUsersObj: function(req, res) {
    User.getAllUsers(req, function(list) {
      var result = {};
      list.forEach(function(item) {
        result[item.id] = item;
      });
      return res.json(result);
    });
  },
  index: function(req, res) {
    var title = LocalizBySess.__("User list", req);
    return res.view({
      layout: sails.config.views.adminLayout,
      controller: sails.controllers.user._config.controller,
      title: title,
      sessionUser: User.getSessionUser(req),
      menuPoints: sails.config.blueprints.publicControllers
    });
  },
  card: function(req, res) {
    User.findOne(parseInt(req.param('id'))).exec(function(err, usercase) {
      if(usercase) usercase.roleImportance = req.session.User.roleImportance;
      return res.view('user/form', {
        layout: sails.config.views.adminLayout,
        controller: sails.controllers.user._config.controller,
        user: usercase,
        title: typeof(usercase) != 'object' ? LocalizBySess.__("Create new user", req) : LocalizBySess.__("Update user", req),
        sessionUser: User.getSessionUser(req),
        menuPoints: sails.config.blueprints.publicControllers,
        action: req.options.action
      });
      req.session.flash = {};
    });
  },
  create: function(req, res, next) {
    User.create(req.params.all(), function userCreated(err, resuser) {
      if(err) {
        req.session.flash = {
          err: err
        };
        return res.redirect('admin/user/card');
      } else {
        User.publishCreate(resuser);
        return res.redirect('admin/user/show/' + resuser.id);
      }
    });
  },
  show: function(req, res, next) {
    User.findOne(parseInt(req.param('id'))).exec(function foundUser(err, user) {
      Roles.getRoleByID(user.role, 'name', function(err, roleName) {
        if(err) {
          sails.log.error(err);
          return res.view('500');
        }
        if(!user) return res.view({
          error: LocalizBySess.__("User not found", req)
        }, '404');
        user.role = roleName;
        return res.view({
          layout: sails.config.views.adminLayout,
          user: user,
          controller: sails.controllers.user._config.controller,
          title: LocalizBySess.__("User info", req) + '&nbsp;&laquo;' + user.login + '&raquo;',
          sessionUser: User.getSessionUser(req),
          menuPoints: sails.config.blueprints.publicControllers
        });
      });
    });
  },
  update: function(req, res, next) {
    User.findOne(parseInt(req.param('id'))).exec(function(err, user) {
      var oldPassword = user.password;
      var _params = req.params.all();
      user.login = _params.login;
      user.name = _params.name;
      user.mail = _params.mail;
      user.role = typeof(_params.rid) !== 'undefined' ? parseInt(_params.rid) : parseInt(_params.role);
      user.password = _params.password == "" ? oldPassword : 'newPas';
      var saveCB = function() {
        user.save(function(err) {
          if(err) {
            req.session.flash = {
              err: err
            };
            return res.redirect('admin/user/card/');
          } else {
            User.publishUpdate(user.id, {
              updated: true,
              newUser: user
            });
            return res.redirect('admin/user/show/' + user.id);
          }
        });
      };
      if(user.password === 'newPas') {
        User.cryptPassword(_params.password, function(err, encryptedPassword) {
          if(err) {
            req.session.flash = {
              err: err
            };
            return res.redirect('admin/user/card/');
          }
          user.password = encryptedPassword;
          saveCB();
        });
      } else {
        saveCB();
      }
    });
  },
  remove: function(req, res, next) {
    var _ID = req.param('id');
    User.findOne(_ID).exec(function(err, user) {
      if(err) return next(err);
      User.destroy(_ID).exec(function(err) {
        if(err) {
          sails.log.error(err);
          return res.view('500');
        } else {
          User.publishDestroy(_ID);
          return res.send(200);
        }
      });
    });
  },
  getOnline: function(req, res) {
    return res.json(SockUsers.getList());
  },
  subscribe: function(req, res) {
    /*** Subscribe for users at less roles ***/
    User.find().where({
      role: {
        '>': req.session.User.role
      }
    }).exec(function(err, users) {
      if(err) {
        return next(err);
      }
      User.subscribe(req.socket, users);
      User.watch(req);
      return res.send(200);
    });
  }
};

