module.exports = {
  getTranslateByPhrase: function(req, res) {
    /*** Incoming string type ***/
    var phrase = req.param('phrase');
    return res.json(LocalizBySess.__(phrase, req));
  },
  getPacketTranslate: function(req, res) {
    /*** Incomin array type ***/
    var packet = req.param('packet');
    var result = {};
    packet.forEach(function(word) {
      result[word] = LocalizBySess.__(word, req);
    });
    return res.json(result);
  }
};

