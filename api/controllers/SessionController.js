var bcrypt = require('bcrypt');
module.exports = {
  _config: {
    controller: 'session',
  },
  new: function(req, res) {
    res.locals.flash = _.clone(req.session.flash);
    req.session.flash = {};
    return res.view({
      title: "Login page",
      layout: sails.config.views.adminLayout,
      controller: sails.controllers.session._config.controller,
      usermenu: res.usermenu ? res.usermenu : null
    });
  },
  create: function(req, res) {
    if(!req.param('login') || !req.param('password')) {
      req.session.flash = {
        err: ['You must enter both login and password']
      };
      return res.redirect('/admin/session/new');
    }
    User.findOne().where({
      login: req.param('login')
    }).exec(function(err, user) {
      if(err) {
        sails.log.error(err);
        return res.view('500');
      }
      if(!user || user.length == 0) {
        req.session.flash = {
          err: ['Wrong username']
        };
        return res.redirect('/admin/session/new');
      }
      bcrypt.compare(req.param('password'), user.password, function(err, valid) {
        if(err) {
          sails.log.error(err);
          return res.view('500');
        }
        if(!valid) {
          req.session.flash = {
            err: ['Wrong password']
          };
          return res.redirect('/admin/session/new');
        }
        //if(SockUsers.getList().indexOf(user.id) >= 0) {
          //req.session.flash = {
            //err: ['Double login doesn\'t allow']
          //};
          //return res.redirect('/admin/session/new');
        //}
        Roles.findOne({
          id: user.role
        }).exec(function(err, userRole) {
          if(err) {
            sails.log.error(err);
            return res.view('500');
          }
          var lang = req.param('lang');
          if(typeof(lang) !== 'undefined' && lang) {
            req.session.lang = lang !== 'default' ? lang : sails.config.i18n.defaultLocale;
            res.cookie('defaultLocale', req.session.lang);
          }
          req.session.authenticated = true;
          req.session.User = user;
          req.session.User.roleImportance = userRole.importance;

          //SockUsers.addUser(req.session.User);
          User.publishUpdate(user.id, {
            online: true
          });
          var dateFormat = require('dateformat');
          var expirationTime = 24 * 60 * 60000;
          req.session.User.expire = new Date(dateFormat(new Date(), "isoDateTime")).getTime() + expirationTime;
          return res.redirect('/admin');
        });
      });
    });
  },
  exit: function(req, res) {
    if(typeof(req.session.User) !== 'undefined') {
      var _id = req.session.User.id;
      User.publishUpdate(_id, {
        online: false
      });
      SockUsers.removeUser(req.session.User);
      req.session.authenticated = false;
      req.session.destroy();
    }
    return res.redirect('/admin/session/new');
  }
};

