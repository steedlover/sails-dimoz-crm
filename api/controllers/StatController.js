module.exports = {
  _config: {
    controller: 'stat'
  },
  index: function(req, res) {
    res.view({
      layout: sails.config.views.adminLayout,
      controller: sails.controllers.stat._config.controller,
      title: LocalizBySess.__('Statistics page', req),
      sessionUser: User.getSessionUser(req),
      menuPoints: sails.config.blueprints.publicControllers
    });
  }
};

