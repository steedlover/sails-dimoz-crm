module.exports = {
  _config: {
    controller: 'actions'
  },
  getActionByID: function(req, res) {
    Actions.find(req.param('id')).populate('owner').populate('log').exec(function(err, action) {
      if(err) {
        sails.log.error(err)
        return res.redirect(500);
      }
      var result = action[0];
      return typeof(result) !== 'undefined' ? res.json(result) : res.json(null);
    });
  },
  getAllRows: function(req, res) {
    var selQuery = "SELECT \
        a2.is_dbl, \
        log.datetime AS created, \
        log2.stat AS status, \
        a1.* \
      FROM actions AS a1 \
      JOIN \
        (SELECT DISTINCT t2.dbl, t2.is_dbl \
         FROM `actions` \
         JOIN  \
          ( \
            SELECT \
            phone AS gphone, \
            min( id ) AS dbl, \
            IF(min(id) < max(id),1,0) AS is_dbl \
            FROM actions \
            GROUP BY gphone \
          ) AS t2 ON actions.phone = t2.gphone \
        ) as a2 \
      ON a1.id = a2.dbl \
      LEFT JOIN actions_log AS log \
      ON a1.id = log.action_id AND log.stat = (SELECT MIN(s.stat) FROM actions_log AS s WHERE action_id = a1.id) \
      LEFT JOIN actions_log AS log2 \
      ON a1.id = log2.action_id AND log2.stat = (SELECT MAX(s1.stat) FROM actions_log AS s1 WHERE action_id = a1.id) \
      WHERE a1.archive = 0 \
      ORDER BY a1.id DESC;";
    Actions.query(selQuery, function(err, actions) {
      if(err) {
        sails.log.error(err)
        return res.redirect(500);
      }
      return res.json(actions);
    });
  },
  getDoubles: function(req, res) {
    var _phone = req.param('phone');
    Actions.find({
        phone: _phone
      })
      //.populate('owner')
      .populate('log').exec(function(err, list) {
        if(err) {
          sails.log.error(err)
          return res.redirect(500);
        }
        list.forEach(function(item, index) {
          /*** Make action single ***/
          item.is_dbl = 0;
          item.status = item.log.length > 0 ? item.log[item.log.length - 1].stat_schema : null;
          item.created = item.log.length > 0 ? item.log[0].datetime : null;
          /*** Convert object to object is the trick,
           * but otherwise delete doesn't work
           * ***/
          item = item.toObject();
          delete item.log;
          list[index] = item;
        });
        return res.json(list);
      });
  },
  getStats: function(req, res) {
    Stat_schema.getStatObj(function(response) {
      return res.json(response);
    });
  },
  index: function(req, res) {
    var title = 'Actions list';
    return res.view({
      layout: sails.config.views.adminLayout,
      controller: sails.controllers.actions._config.controller,
      title: title,
      sessionUser: User.getSessionUser(req),
      menuPoints: sails.config.blueprints.publicControllers
    });
  },
  postJson: function(req, res) {
    var row = typeof(req.param('data')) !== 'undefined' && typeof(req.param('data')) === 'object' && req.param('data') ? req.param('data') : null;
    if(row !== null && typeof(row.visit_params) !== 'undefined' && row.visit_params) {
      row.visit_params = row.visit_params.toLowerCase();
    }
    if(row === null) {
      return res.json({
        error: 'Wrong input',
        status: 500
      });
    }
    Actions.create(row, function(err, result) {
      /*** Check for doubles for action ***/
      //console.log(result);
      if(err) {
        sails.log.error(err);
        return res.json(err);
      }
      Stat_schema.find().exec(function(err, stats) {
        /***
         * New status for action
         * first in the stat list
         * ***/
        var newStat = stats[0];
        Actions_log.create({
          action_id: result.id,
          stat: newStat.id
        }, function(err, em) {
          if(err) {
            sails.log.error(err);
            return res.json(err);
          }
          Actions.findOne(result.id).populate('log', {
            stat: newStat.id
          }).exec(function(err, newAction) {
            if(err) {
              sails.log.error(err);
              return res.json(err);
            }
            /*** Check for the first status only
             * because we just create the action
             ***/
            newAction.created = newAction.log[0].datetime;
            Actions.publishCreate(newAction);
            return res.json(newAction);
          });
        });
      });
    });
  },
  subscribe: function(req, res) {
    /*** Subscribes for all actions ***/
    Actions.find().exec(function(err, users) {
      if(err) {
        return next(err);
      }
      Actions.subscribe(req.socket, users);
      Actions.watch(req);
      return res.send(200);
    });
  }
};

