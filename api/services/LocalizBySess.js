module.exports = {
  __: function(word, req) {
    var lang = sails.config.i18n.defaultLocale; //Global default locale
    var session = req.session;
    // If locale found in session (Normal behavior)
    if(typeof(session) !== 'undefined' && typeof(session.lang) !== 'undefined') {
      lang = session.lang;
    } else if(typeof(req.cookies.defaultLocale) !== 'undefined' && req.cookies.defaultLocale !== '') {
      lang = req.cookies.defaultLocale;
    }
    //var index = word.indexOf('%s');
    //if(index >= 0) word = word.replace('%s', '');
    return sails.__({
      phrase: word,
      locale: lang
    });
  }
};

