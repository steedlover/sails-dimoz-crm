module.exports = {
  isEmpty: function(obj) {
    if(obj == null) return true;
    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if(obj.length > 0) return false;
    if(obj.length === 0) return true;
    for(var key in obj) {
      if(hasOwnProperty.call(obj, key)) return false;
    }
    return true;
  }
};

