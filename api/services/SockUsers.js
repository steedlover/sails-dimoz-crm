var socketUsersList = [];
module.exports = {
  getList: function() {
    return socketUsersList;
  },
  addUser: function(user) {
    if(socketUsersList.length === 0
      || socketUsersList.indexOf(user.id) < 0)
      socketUsersList.push(user.id);
  },
  removeUser: function(user) {
    if(socketUsersList.length > 0
      && socketUsersList.indexOf(user.id) >= 0)
      socketUsersList.splice(socketUsersList.indexOf(user.id), 1);
  }
};

