module.exports = function(req, res, next) {
  var _error = false;
  if(req.session.authenticated == 'undefined'
    || !req.session.authenticated
    || !req.session.User
    || req.session.User.length == 0) {
    _error = true;
  } else {
    var dateFormat = require('dateformat');
    var curDateSecs = new Date(dateFormat(new Date(), "isoDateTime")).getTime();
    if(req.session.User.expire - curDateSecs <= 0) {
      req.session.flash = {
        err: ['Session expired']
      };
      _error = true;
      req.session.authenticated = false;
      SockUsers.removeUser(req.session.User);
      delete req.session.User;
    }
  }
  return !_error ? next() : res.redirect('/admin/session/new');
};

