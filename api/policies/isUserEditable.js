module.exports = function(req, res, next) {
  var userID = req.param('id');
  if(!req.session || !req.session.User) {
    return res.redirect('/admin/session/new');
  } else if(userID) {
    /* It's mean the user configured themself, but NOT remove */
    if(userID == req.session.User.id && req.options.action != 'remove') {
      return next();
    } else {
      Roles.getRoleByID(req.session.User.role, null, function(err, role) {
        Roles.getRolesObj(role.importance, function(err, roles) {
          if(err) {
            sails.log.error(err);
            return res.view('500');
          }
          req.roles = roles;
          User.findOne(userID).exec(function(err, user) {
            if(err || !user) {
              if(err) sails.log.error(err);
              if(!user) sails.log.error(userID + ' user doesn\'t exists');
              if(req.isSocket) return res.json({
                statusCode: 404,
                message: 'User doesn\'t exists'
              });
              else return res.view('500');
            }
            //** If user role id in available roles Array **//
            if(roles.arr.indexOf(user.role) >= 0 || req.session.User.superadmin == true) return next();
            else return res.view('403');
          });
        });
      });
    }
  } else {
    return next();
  }
};

