module.exports = function(req, res, next) {
  User.find().limit(1).exec(function(err, user) {
    if(err) {
      sails.log.error(err);
      return res.view('500');
    }
    user = user[0];
    req.session.User.superadmin = user.login == req.session.User.login ? true : false;
    return req.session.User.superadmin ? next() : res.view('403');
  });
};

