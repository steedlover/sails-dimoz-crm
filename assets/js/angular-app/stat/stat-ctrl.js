angularApp.controller('StatCtrl', ['$scope', '$http', 'mozConfig', 'mozStat',
  function($scope, $http, mozConfig, mozStat) {
    $scope.actionsBySource = [];
    $scope.actionsByTerm = [];
    $scope.rowsCount = 0;
    $scope.statLoading = true;
    $scope.getStatList = function() {
      var _url = '/admin/actions/list';
      socket.get(_url, function(response) {
        if(response.statusCode && response.statusCode != 200) {
          $scope.loading = false;
          $scope.$digest();
          return console.error('GetStatList error: ' + response.statusCode);
        }
        $scope.all = response.length;
        $scope.actionsBySource = {};
        $scope.actionsByTerm = {};
        $scope.statLoading = true;
        response.forEach(function(val, index) {
          if(!mozConfig.isObjEmpty(val.visit_params)) {
            var _params = JSON.parse(val.visit_params);
            $scope.actionsBySource = mozStat.addChartElement($scope.actionsBySource, _params.utm_source, 1);
            $scope.actionsByTerm = mozStat.addChartElement($scope.actionsByTerm, _params.utm_term, 1);
          }
          //$scope.actionsBySource = mozStat.addChartElement($scope.actionsBySource, 'undefined', 1, {
          //fillColor: "rgba(238,238,238,1)",
          //strokeColor: "rgba(238,238,238,1)",
          //pointColor: "rgba(238,238,238,1)"
          //}
        });
        $scope.statLoading = false;
        $scope.$digest();
      });
    };
    $scope.addSourceElement = function(label, value) {
      $scope.actionsBySource = mozStat.addChartElement($scope.actionsBySource, label, value);
    };
    // Start action
    $scope.getStatList();
  }
]);

