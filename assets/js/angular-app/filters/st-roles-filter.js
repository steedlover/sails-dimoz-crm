angularApp.filter('stRolesFilter', ['$filter',
  function($filter) {
    var filterFilter = $filter('filter');
    var standardComparator = function standardComparator(obj, text) {
      text = ('' + text).toLowerCase();
      return('' + obj).toLowerCase().indexOf(text) > -1;
    };
    return function stRolesFilter(array, expression) {
      function customComparator(actual, expected) {
        if(typeof(expected) === 'object') {
          var predicate = Object.keys(expected)[0];
          /*** Search by role ***/
          if(typeof(expected[predicate]) !== 'undefined') {
            if(typeof(actual) !== 'undefined') {
              if(expected[predicate] === '') {
                return true;
              } else if(typeof(actual) === 'number') {
                if(actual != expected[predicate]) {
                  return false;
                }
              } else if(typeof(actual) === 'string') {
                if(actual.toLowerCase() != expected[predicate].toLowerCase()) {
                  return false;
                }
              }
            }
            return true;
          }
          /*** ************* ***/
        }
        return standardComparator(actual, expected);
      };
      return filterFilter(array, expression, customComparator);
    };
  }
]);

