angularApp.controller('ActionsCtrl', ['$scope', '$timeout', '$cookies', '$compile', 'mozConfig', 'mozStat', 'mozSocket', 'mozTranslate',
  function($scope, $timeout, $cookies, $compile, mozConfig, mozStat, mozSocket, mozTranslate) {
    $scope.loading = true;
    $scope.rows = null;
    $scope.newrows = [];
    $scope.newrows_displayed = [];
    $scope.rowsCount = 0;
    $scope.displayed = [];
    $scope.ctrlName = 'actions';
    $scope.onPageCookieName = mozConfig.getCookieName($scope.ctrlName, 'OnPage');
    $scope.pageNumCookieName = mozConfig.getCookieName($scope.ctrlName, 'PageNum');
    $scope.pageNum = mozConfig.getCookieVar($scope.ctrlName, 'PageNum', 1);
    $scope.initOnPage = function() {
      $scope.onPage = mozConfig.getCookieVar($scope.ctrlName, 'OnPage', mozConfig.onPageDef);
      $scope.setOnPage($scope.onPage);
      $scope.getActionsList();
    };
    /*** **** Translation block **** ***/
    $scope.finTranslate = false;
    $scope.vocabulary = mozTranslate.getTranslate();
    $scope.$on('translateReady', function(event, res) {
      $scope.vocabulary = res.data;
      $scope.finTranslate = true;
    });
    $scope.$watch('vocabulary', function(newVal) {
      if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
        $scope.finTranslate = true;
      }
    });
    /*** *************************** ***/
    $scope.setOnPage = function(num) {
      $scope.onPage = parseInt(num);
      $cookies.put($scope.onPageCookieName, num);
    };
    $scope.$on('changeCurPage', function(event, args) {
      var newPage = args.page;
      $cookies.put($scope.pageNumCookieName, newPage);
    });
    $scope.$on('addDoublesTab', function(event, params) {
      $scope.doubles[params.id] = params.doubles;
      $scope.doubles_displayed[params.id] = [];
      $scope.tabs.push({
        type: 'double',
        title: $scope.vocabulary['Doubles'] + ' #' + params.id,
        name: 'doubles_' + params.id,
        collection: $scope.doubles[params.id],
        displayed: $scope.doubles_displayed[params.id]
      });
    });
    $scope.getTabIdByName = function(title) {
      var regExp = /#(\d+)/ig;
      var regArr = regExp.exec(title);
      return parseInt(regArr[1]);
    };
    $scope.delTab = function(event, tabTitle) {
      var tabID = $scope.getTabIdByName(tabTitle);
      event.preventDefault();
      event.stopPropagation();
      if(typeof($scope.doubles[tabID]) === 'object') {
        angular.forEach($scope.tabs, function(obj, index) {
          /*
           * First object is a full actions list
           * and it is undeletable
           * ***/
          if(index > 0 && obj.type === 'double') {
            if($scope.getTabIdByName(obj.title) === tabID) $scope.tabs.splice(index, 1);
          }
        });
        delete $scope.doubles[tabID];
        delete $scope.doubles_displayed[tabID];
        /***
         * Tell to directive st-action-double-icon
         * about deleting tab for exclude it number of
         * existing tabs array ***/
        $scope.$broadcast('delTab', tabID);
      }
    };
    $scope.getActionsList = function() {
      mozSocket.getDataBySocket('/admin/actions/list', 'GetActionsList').then(function(response) {
        return $scope.rows = response;
      });
    };
    $scope.$watchGroup(['rows', 'finTranslate'], function(newVals, oldVals) {
      var ready = true;
      if(typeof(newVals[0]) === 'undefined' || !newVals[0] || typeof(newVals[1]) === 'undefined' || newVals[1] === false) {
        ready = false;
      }
      if(ready === true) {
        $scope.rowsCount = $scope.rows.length;
        $scope.loading = false;
        $scope.doubles = [];
        $scope.doubles_displayed = [];
        $scope.tabs = [{
          type: 'default',
          title: $scope.vocabulary['Full list'],
          name: 'full_list',
          collection: $scope.rows,
          displayed: $scope.displayed
        }, {
          type: 'bells',
          title: $scope.vocabulary['New'],
          name: 'new_list',
          collection: $scope.newrows,
          displayed: $scope.newrows_displayed
        }];
        $timeout(function() {
          $scope.$broadcast('setCurPage', {
            page: $scope.pageNum
          });
        });
      }
    });
    $scope.addItem = function() {
      var obj = {
        id: 200,
        fullname: 'Вася Пупкин',
        type: 'Запрос информации',
        created: '2015-08-17 14:10:23',
        status: 2
      };
      $scope.rows.unshift(obj)
      $scope.newrows.unshift(obj)
    };
    socket.on('actions', function(jwres) {
      if(typeof(jwres.verb) !== 'undefined' && jwres.verb == 'created') {
        /*** creating user ***/
        var newAction = jwres.data;
        $scope.rows.unshift(newAction);
        $scope.newrows.unshift(newAction);
        $scope.$digest();
      }
    });
    socket.get('/admin/actions/subscribe');
  }
]);

