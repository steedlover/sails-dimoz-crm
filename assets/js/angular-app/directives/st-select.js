angular.module('mozStSelect', []).directive('stSelect', ['mozConfig', 'mozTranslate',
  function(mozConfig, mozTranslate) {
    return {
      restrict: 'E',
      require: '^stTable',
      scope: {
        collection: '=',
        object: '=',
        predicate: '@',
      },
      template: '<select id="{{ predicate }}SelectPicker" selectpicker data-style="btn-primary" ng-model="selectedOption" ng-change="optionChanged(selectedOption)" ng-options="label.value as label.title for label in labels" ng-if="finTranslate"></select>',
      link: function(scope, element, attrs, table) {
        var addObject = scope.object;
        /*** **** Translation block **** ***/
        scope.finTranslate = false;
        scope.vocabulary = mozTranslate.getTranslate();
        scope.$on('translateReady', function(event, res) {
          scope.vocabulary = res.data;
          scope.finTranslate = true;
        });
        scope.$watch('vocabulary', function(newVal) {
          if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
            scope.finTranslate = true;
          }
        });
        /*** *************************** ***/
        scope.$watch('finTranslate', function(newVal) {
          if(newVal === true) {
            /*** If "object" object defined at html ***/
            scope.labels = [{
              value: 0,
              title: scope.vocabulary['All']
            }];
            if(addObject && typeof(addObject) === 'object') {
              for(var key in addObject) {
                scope.labels.push({
                  value: key,
                  title: addObject[key]
                });
              }
            } else if(typeof(scope.object) === 'undefined') {
              scope.collection.forEach(function(value, index) {
                if(typeof(value[scope.predicate]) !== 'undefined' && value[scope.predicate] && !mozConfig.isObjectInArray(scope.labels, 'title', value[scope.predicate])) {
                  scope.labels.push({
                    value: value[scope.predicate],
                    title: value[scope.predicate]
                  });
                }
              });
            }
            scope.selectedOption = 0;
            scope.optionChanged = function(selectedOption) {
              var query = {};
              query[scope.predicate] = selectedOption;
              if(query[scope.predicate] == 0) {
                query[scope.predicate] = '';
              }
              table.search(query, scope.predicate);
            };
            scope.optionChanged(scope.selectedOption);
          }
        });
      }
    };
  }
]);

