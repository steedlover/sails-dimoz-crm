angular.module('mozStUserRow', []).directive('stUserRow', ['$timeout', '$animate',
  function($timeout, $animate) {
    return {
      require: '^stTable',
      restrict: 'A',
      link: function(scope, element, attrs) {
        /*** Activate on changing user status (online/offline) ***/
        scope.$on('updateUserStatus', function(event, args) {
          var _uid = [];
          if(typeof(args.id) === 'number') {
            _uid.push(args.id);
          } else if(typeof(args.id) === 'object') {
            _uid = args.id;
          }
          var _online = args.online;
          if(_uid.indexOf(parseInt(attrs.uid)) >= 0) {
            var statBlock = element.children().children();
            if(_online === true) statBlock.addClass('online');
            else statBlock.removeClass('online');
          }
        });
        /*** Activate on changing user info ***/
        scope.$on('updateUserData', function(event, args) {
          var _uid = args.id;
          if(attrs.uid == _uid) {
            element.addClass('updated');
            $timeout(function() {
              element.removeClass('updated');
            }, 500);
          }
        });
      }
    };
  }
]);

