angular.module('mozStActionDoubleIcon', []).directive('stActionDoubleIcon', ['mozSocket',
  function(mozSocket) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        stActionDoubleIcon: "@"
      },
      template: '<div class="glyphicon double-icon" ng-class="doubleTagClass(dbl)"></div>',
      link: function(scope, element, attrs, controllers) {
        var params = scope.$eval(scope.stActionDoubleIcon);
        scope._id = params.id;
        scope.dbl = params.dbl; // boolean type
        scope.linkTitle = params.title;
        var origPhone = params.phone;
        scope.localDoubles = [];
        scope.$on('delTab', function(event, tabId) {
          var _index = scope.localDoubles.indexOf(tabId.toString());
          if(_index >= 0) delete scope.localDoubles[_index];
        });
        scope.doubleTagClass = function(val) {
          if(val > 0) return 'glyphicon-tags clickable';
          else if(typeof(val) === 'undefined' || val == 0) return 'glyphicon-tag';
        };
        element.on('click', function(event) {
          if(element.hasClass('clickable') === true) {
            if(scope.localDoubles.indexOf(scope._id.toString()) < 0) {
              mozSocket.getDataBySocket('/admin/actions/doubles', 'GetActionsDoubles', {
                phone: origPhone
              }).then(function(response) {
                if(response.length > 0) {
                  var doublesArr = [];
                  angular.forEach(response, function(val) {
                    if(val.id !== parseInt(scope._id)) doublesArr.push(val);
                  });
                  scope.$emit('addDoublesTab', {
                    id: scope._id,
                    doubles: doublesArr
                  });
                  scope.localDoubles.push(scope._id);
                }
                return;
              });
            }
          }
        });
        /*** Remake element to the link ***/
        var linkStyle = scope.dbl === true ? 'position: relative;' : '';
        linkStyle = linkStyle !== '' ? 'style="' + linkStyle + '"' : '';
        if(scope.dbl === true) element.wrap('<a href="" ' + linkStyle + ' onClick="return false;" title="' + scope.linkTitle + '" />');
      }
    };
  }
]);

