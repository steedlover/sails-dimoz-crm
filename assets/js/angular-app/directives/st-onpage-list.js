angular.module('mozStOnPageList', []).directive('onpageSelect', ['mozTranslate', 'mozConfig',
  function(mozTranslate, mozConfig) {
    return {
      restrict: 'AE',
      templateUrl: '../templates/pagination/st-onpage-select.html',
      templateNamespace: 'html',
      link: function(scope, element, attrs) {
        /*** Translation block ***/
        scope.finTranslate = false;
        scope.vocabulary = mozTranslate.getTranslate();
        scope.$on('translateReady', function(event, res) {
          scope.vocabulary = res.data;
          scope.finTranslate = true;
        });
        scope.$watch('vocabulary', function(newVal) {
          if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
            scope.finTranslate = true;
          }
        });
        /*** *************************** ***/
      }
    };
  }
]);

