angular.module('mozDetailsWindow', []).directive('detailsWindow', ['$timeout',
  function($timeout) {
    return {
      strict: 'A',
      templateUrl: '/templates/details/popup.html',
      controller: ['$scope', 'mozSocket', 'mozConfig', 'mozTranslate',
        function($scope, mozSocket, mozConfig, mozTranslate) {
          /*** Translation block ***/
          $scope.finTranslate = false;
          $scope.vocabulary = mozTranslate.getTranslate();
          $scope.$on('translateReady', function(event, res) {
            $scope.vocabulary = res.data;
            $scope.finTranslate = true;
          });
          $scope.$watch('vocabulary', function(newVal) {
            if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
              $scope.finTranslate = true;
            }
          });
          /*** *************************** ***/
          $scope.loading = true;
          $scope.getData = function() {
            if($scope.reqType === 'actions') {
              mozSocket.getDataBySocket('/admin/actions/action/' + $scope.ID, 'GetAction').then(function(response) {
                return $scope.data = response;
              });
            }
          };
          $scope.$watch('data', function(newVal) {
            if(typeof(newVal) === 'object') $scope.loading = false;
          });
        }
      ],
      link: function(scope, element, attrs) {
        scope.reqType = attrs.type;
        scope.$on('mozShowDetails', function(event, args) {
          /***
           * args - object type
           * i.e { id: id }
           * ***/
          scope.ID = args.id;
          if(!element.hasClass('active')) element.addClass('active');
          scope.getData();
        });
        scope.removeWindow = function() {
          if(element.hasClass('active')) element.removeClass('active');
        };
      }
    };
  }
]);

