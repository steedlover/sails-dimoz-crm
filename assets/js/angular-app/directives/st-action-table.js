angular.module('mozStActionTable', []).directive('stActionTable', ['mozConfig', 'mozTranslate', '$window', 'mozSocket',
  function(mozConfig, mozTranslate, $window, mozSocket) {
    return {
      restrict: 'E',
      scope: {
        rows: "=collection",
        displayed: "=displayed"
      },
      templateUrl: '../templates/actions/index.html',
      controller: function($scope, $element) {
        $scope.loading = true;
        $scope.rowClick = function(event, id) {
          /***
           * two parameters by arguments
           * and $event the first
           * ***/
          var row = angular.element(event.target).parent();
          row.parent().children().removeClass('clicked');
          row.addClass('clicked');
          var sel = $window.getSelection();
          sel.removeAllRanges();
          $scope.$emit('mozShowDetails', {
            id: id
          });
        };
        $scope.objFromJsonStr = function(str) {
          return str ? JSON.parse(str) : null;
        };
        $scope.getSourceName = function(name) {
          return mozConfig.getCapitalizeFirstLetter(name);
        };
        /*** **** Translation block **** ***/
        $scope.finTranslate = false;
        $scope.vocabulary = mozTranslate.getTranslate();
        $scope.$on('translateReady', function(event, res) {
          $scope.vocabulary = res.data;
          $scope.finTranslate = true;
        });
        $scope.$watch('vocabulary', function(newVal) {
          if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
            $scope.finTranslate = true;
          }
        });
        /*** *************************** ***/
        $scope.isAtWork = function(owner) {
          var owner = typeof(owner) !== 'undefined' ? parseInt(owner) : 0;
          return owner > 0 ? true : false;
        };
        $scope.getStats = function() {
          mozSocket.getDataBySocket('/admin/actions/stats', 'GetStats').then(function(response) {
            return $scope.stats = response;
          });
        };
        $scope.getUsers = function() {
          mozSocket.getDataBySocket('/admin/user/getobj', 'GetUsers').then(function(response) {
            return $scope.users = response;
          });
        };
        $scope.getStats();
        $scope.getUsers();
        $scope.$watchGroup(['users', 'stats', 'finTranslate'], function(newVals, oldVals) {
          var ready = true;
          newVals.forEach(function(item) {
            if(typeof(item) === 'undefined' || !item) ready = false;
          });
          if(ready === true) $scope.loading = false;
        });
      },
      link: function(scope) {
        scope.pagiVizLinks = mozConfig.pagiVizLinksDef;
        scope.onPage = mozConfig.onPageDef;
      }
    };
  }
]);

