angular.module('mozBell', []).directive('stAlarmBell', ['$timeout',
  function($timeout) {
    return {
      restrict: 'A',
      scope: {
        stAlarmBell: "="
      },
      link: function(scope, element, attrs) {
        scope.$watch('stAlarmBell', function(newVal, oldVal) {
          if(newVal !== 'undefined' || newVal > oldVal) {
            element.addClass('alarm');
            $timeout(function() {
              element.removeClass('alarm');
            }, 3000);
          }
        });
      }
    };
  }
]);

