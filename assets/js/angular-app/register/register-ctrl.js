angularApp.controller('RegCtrl', ['$scope', '$http', '$timeout', 'mozConfig',
  function($scope, $http, $timeout, mozConfig) {
    $scope.signup = {
      "visit_params": mozConfig.QueryString()
    };
    $timeout(function() {
      $scope.form = 1;
    });
    $scope.sendtime = 2;
    $scope.oneWordPattern = (function() {
      var regexp = /^[А-яA-zёЁ]+$/;
      return {
        test: function(value) {
          return regexp.test(value);
        }
      };
    })();
    $scope.emailPattern = (function() {
      var regexp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return {
        test: function(value) {
          return regexp.test(value);
        }
      }
    })();
    $scope.less = function() {
      $scope.form--;
    };
    $scope.formSubmit = function(test) {
      var test = typeof(test) === 'undefind' || !test ? false : true;
      $scope.form++;
      if(test) {
        if($scope.form > $scope.sendtime) {
          $scope.sendJson();
        }
      }
      return;
    };
    $scope.getCode = function() {
      $scope.phoneCode = Math.random().toString(36).slice(-8);
      return;
    };
    $scope.sendJson = function() {
      var visit_params = !mozConfig.isObjEmpty($scope.signup.visit_params) ? JSON.stringify($scope.signup.visit_params) : null;
      var result = {
        "fullname": $scope.signup.secondname + ' ' + $scope.signup.firstname + ' ' + $scope.signup.patronymic,
        "mailbox": $scope.signup.mailbox,
        "phone": $scope.signup.phone,
        "type": "Запрос информации",
      };
      if(visit_params !== null) result["visit_params"] = visit_params;
      $http.post('/admin/actions/post.json', {
        data: result
      }).then(function(response) {
        location.href = '/admin/actions';
      });
    };
  }
]);

