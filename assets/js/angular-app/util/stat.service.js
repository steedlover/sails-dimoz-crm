angularApp.factory('mozStat', ['$cookies', 'mozConfig',
  function($cookies, mozConfig) {
    var service = {};
    service.addChartElement = function(obj, label, value, colour) {
      var colour = typeof(colour) === 'object' && colour ? colour : '';
      var _label = mozConfig.getCapitalizeFirstLetter(label);
      if(typeof(obj.colours) === 'undefined') {
        obj.colours = [];
      }
      if(typeof(obj.labels) === 'undefined') {
        obj.labels = [];
      }
      if(typeof(obj.data) === 'undefined') {
        obj.data = [];
      }
      if(obj.labels.indexOf(_label) < 0) {
        obj.labels.push(_label);
        obj.data.push(value);
        obj.colours.push(colour);
      } else {
        var index = obj.labels.indexOf(_label);
        obj.data[index] = obj.data[index] + value;
        obj.colours[index] = colour;
      }
      return obj;
    };
    return service;
  }
]);

