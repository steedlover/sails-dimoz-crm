angularApp.factory('mozTranslate', ['$rootScope', 'mozConfig', 'mozSocket',
  function($rootScope, mozConfig, mozSocket) {
    var service = {};
    service.vocabulary = {};
    service.busy = false;
    service.wordsToTranslate = ["Show by", "per page", "Page", "Pg.", "of", "At the last page", "At the first page", "Next page", "Previous page", "Are you shure to remove", "All", "Creation date", "Action type", "Full info", "Undefined", "Ingoing", "Search", "by action data", "Full list", "New", "Doubles", "Show doubles", "Delete", "Manage", "Loading", "Status", "Owner", "Climb"];
    service.getTranslate = function() {
      if(mozConfig.isObjEmpty(service.vocabulary) && !service.busy) {
        service.busy = true;
        return service.translatePacketWords();
      } else {
        return service.vocabulary;
      }
    };
    service.translatePacketWords = function() {
      var _url = '/admin/locale/packet';
      var _data = {
        packet: service.wordsToTranslate
      };
      mozSocket.getDataBySocket(_url, 'GetPacketTranslate', _data).then(function(response) {
        //console.log('translate');
        for(var key in response) {
          service.vocabulary[key] = response[key];
        }
        $rootScope.$broadcast('translateReady', {
          data: service.vocabulary
        });
        service.busy = false;
        return service.vocabulary;
      });
    };
    return service;
  }
]);

