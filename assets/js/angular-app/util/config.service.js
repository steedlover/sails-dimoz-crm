angularApp.factory('mozConfig', ['$cookies',
  function($cookies) {
    var service = {};
    service.onPageDef = 5;
    service.pagiVizLinksDef = 5;
    service.getCookieName = function(ctrlName, varName) {
      return ctrlName + varName + '';
    };
    service.getCookieVar = function(ctrlName, varName, defVal) {
      var cookieVar = $cookies.get(service.getCookieName(ctrlName, varName));
      var result = typeof(cookieVar) !== 'undefined' && cookieVar ? cookieVar : defVal;
      return result;
    };
    service.isObjectInArray = function(array, predicate, value) {
      return array.map(function(e) {
        return e[predicate];
      }).indexOf(value) > 0;
    };
    service.getObjectIndexInArray = function(array, predicate, value) {
      var result = null;
      angular.forEach(array, function(obj) {
        if(obj[predicate] === value) {
          result = array.indexOf(obj);
        }
      });
      return result;
    };
    service.isObjEmpty = function(obj) {
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      // null and undefined are "empty"
      if(obj == null) return true;
      // Assume if it has a length property with a non-zero value
      // that that property is correct.
      if(obj.length > 0) return false;
      if(obj.length === 0) return true;
      // Otherwise, does it have any properties of its own?
      // Note that this doesn't handle
      // toString and valueOf enumeration bugs in IE < 9
      for(var key in obj) {
        if(hasOwnProperty.call(obj, key)) return false;
      }
      return true;
    };
    service.getCapitalizeFirstLetter = function(word) {
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    };
    service.objectsUpdateByKeys = function(dest, orig, excludeArr) {
      Object.keys(dest).forEach(function(value, index) {
        var newVal = typeof(orig[value]) !== 'undefined' && orig[value] && excludeArr.indexOf(value) < 0 ? orig[value] : dest[value];
        dest[value] = newVal;
      });
      return dest;
    };
    service.QueryString = function() {
      // This function is anonymous, is executed immediately and
      // the return value is assigned to QueryString!
      var query_string = {};
      if(typeof(window.location.search) === 'string' && window.location.search !== '') {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for(var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          // If first entry with this name
          if(typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
          } else if(typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
          }
        }
      }
      return query_string;
    };
    return service;
  }
]);

