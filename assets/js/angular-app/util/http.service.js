angularApp.factory('mozSocket', ['$q', '$http',
  function($q, $http) {
    var service = {};
    service.getDataBySocket = function(url, errName, sendData) {
      if(typeof(sendData) === 'undefined' || !sendData) {
        var sendData = null;
      }
      var deferred = $q.defer();
      //$http.post(url, sendData)
      //.success(function(response) {
      //deferred.resolve(response);
      //})
      //.error(function(msg, code) {
      //deferred.reject(msg);
      //console.error(errName, code);
      //});
      socket.get(url, sendData, function(response) {
        if(response.statusCode && response.statusCode != 200) {
          return console.error(errName + ' error: ' + response.statusCode);
        }
        deferred.resolve(response);
      });
      return deferred.promise;
    };
    return service;
  }
]);

