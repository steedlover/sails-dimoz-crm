var angularApp = angular.module('angularApp', ['ui.mask', 'ui.bootstrap', 'angular-bootstrap-select', 'ngAnimate', 'smart-table', 'ngRoute', 'ngCookies', 'mozStOnPageList', 'mozStUserRow', 'mozStActionDoubleIcon', 'mozStSelect', 'mozStActionTable', 'mozBell', 'mozDetailsWindow', 'chart.js']).controller('defaultCtrl', ['$scope', function($scope) {
  $scope.model = '';
}]).controller('langCtrl', ['$scope', function($scope) {
  $scope.model = 'default';
}]);

