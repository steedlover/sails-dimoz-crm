angularApp.controller('UserCtrl', ['$scope', '$http', '$timeout', '$cookies', 'mozConfig', 'mozSocket', 'mozTranslate',
  function($scope, $http, $timeout, $cookies, mozConfig, mozSocket, mozTranslate) {
    $scope.loading = true;
    // Paginator variables //
    $scope.rows = null;
    $scope.ctrlName = 'users';
    $scope.onPageCookieName = mozConfig.getCookieName($scope.ctrlName, 'OnPage');
    $scope.pageNumCookieName = mozConfig.getCookieName($scope.ctrlName, 'PageNum');
    // Get page num value from cookies
    $scope.pageNum = mozConfig.getCookieVar($scope.ctrlName, 'PageNum', 1);
    $scope.allPages = 1;
    $scope.displayed = [].concat($scope.rows);
    $scope.roles = null;
    // Num of vizible links with page nums
    $scope.pagiVizLinks = mozConfig.pagiVizLinksDef;
    // ------------------ //
    $scope.initOnPage = function() {
      $scope.onPage = mozConfig.getCookieVar($scope.ctrlName, 'OnPage', mozConfig.onPageDef);
      $scope.setOnPage($scope.onPage);
      $scope.getUsersList();
      $scope.getRoles();
    };
    $scope.setOnPage = function(num) {
      $scope.onPage = parseInt(num);
      $cookies.put($scope.onPageCookieName, num);
    };
    $scope.$on('changeCurPage', function(event, args) {
      var newPage = args.page;
      $cookies.put($scope.pageNumCookieName, newPage);
      $scope.getUsersOnline();
    });
    /*** **** Translation block **** ***/
    $scope.finTranslate = false;
    $scope.vocabulary = mozTranslate.getTranslate();
    $scope.$on('translateReady', function(event, res) {
      $scope.vocabulary = res.data;
      $scope.finTranslate = true;
    });
    $scope.$watch('vocabulary', function(newVal) {
      if(typeof(newVal) !== 'undefined' && !mozConfig.isObjEmpty(newVal)) {
        $scope.finTranslate = true;
      }
    });
    /*** *************************** ***/
    $scope.userRemove = function(row) {
      var _id = row.id;
      var _name = row.login;
      _name = !_name ? _id : _name;
      var result = confirm($scope.vocabulary["Are you shure to remove"] + ' \'' + _name + '\'?');
      if(result) {
        if(_id) {
          var _url = '/admin/user/remove/' + _id;
          mozSocket.getDataBySocket(_url, 'UserRemove');
        }
      } else {
        return false;
      }
    };
    $scope.$watchGroup(['rows', 'roles'], function(newVals) {
      var ready = true;
      newVals.forEach(function(val) {
        if(typeof(val) == 'undefined' || val === null) {
          ready = false;
        }
      });
      if(ready === true) {
        $timeout(function() {
          $scope.$broadcast('setCurPage', {
            page: $scope.pageNum
          });
          $scope.getUsersOnline();
        });
        $scope.loading = false;
      }
    });
    $scope.getUsersList = function() {
      mozSocket.getDataBySocket('/admin/user/list', 'GetUserList')
        .then(function(response) {
          return $scope.rows = response;
        });
    };
    $scope.getRoles = function() {
      mozSocket.getDataBySocket('/admin/roles.list', 'GetRolesList')
        .then(function(response) {
          return $scope.roles = response.length > 0
            ? $scope.$eval(response)
            : [];
        });
    };
    $scope.getUsersOnline = function() {
      mozSocket.getDataBySocket('/admin/user/listonline', 'GetUsersOnline').then(function(response) {
        return $scope.setUserStatusOnline(response, true);
      });
    };
    $scope.setUserStatusOnline = function(_id, _status) {
      $scope.$broadcast('updateUserStatus', {
        id: _id, // Array or number
        online: _status // Boolean param
      });
    };
    socket.on('connect_error', function(err) {
      console.log('Server doesn\'t respond');
    });
    socket.on('user', function(jwres) {
      if(typeof(jwres.verb) !== 'undefined' && jwres.verb == 'updated') {
        /*** If user status(online/offline) changed ***/
        if(typeof(jwres.data.online) !== 'undefined') {
          $scope.setUserStatusOnline(jwres.id, jwres.data.online);
        }
        if(typeof(jwres.data.updated) !== 'undefined' && jwres.data.updated === true) {
          /*** Update user info ***/
          var uid = jwres.id;
          var newUser = jwres.data.newUser;
          var index = mozConfig.getObjectIndexInArray($scope.rows, 'id', uid);
          if(index >= 0) {
            $scope.rows[index] = mozConfig.objectsUpdateByKeys($scope.rows[index], newUser, ["id"]);
            $scope.$digest();
          }
          $scope.$broadcast('updateUserData', {
            id: uid
          });
        }
      } else if(typeof(jwres.verb) !== 'undefined' && jwres.verb == 'destroyed') {
        /*** Deleting user ***/
        var uid = parseInt(jwres.id);
        var index = mozConfig.getObjectIndexInArray($scope.rows, 'id', uid);
        if(index >= 0) {
          $scope.rows.splice(index, 1);
          $scope.$digest();
        }
      } else if(typeof(jwres.verb) !== 'undefined' && jwres.verb == 'created') {
        /*** Creating user ***/
        var newUser = jwres.data;
        $scope.rows.unshift(newUser);
        $scope.$digest();
      }
    });
    socket.get('/admin/User/subscribe');
  }
]);

