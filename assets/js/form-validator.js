var fieldClass = 'field__input';
var pasMinLength = 4;
var pasMaxLength = 12;
var pasChars = '!@#$%^&*';
var errMessages = {
  empty: 'Empty field',
  error: {
    mail: 'Wrong mail format',
    lettersonly: 'This field must contain only letters',
    password: 'Length of the password must be ' + pasMinLength + '-' + pasMaxLength + ' chars and may not content this chars [' + pasChars + ']',
    confirm: 'Input doesn\'t match with password',
    select: 'Role wasn\'t selected',
  },
};

function confirmPassword(str) {
  var password = $('input#password').val();
  if(!password && password != '') return false;
  if(password != str) return false;
  else return true;
};

function checkPas(str) {
  var _test = eval('/[^' + pasChars + ']+/g.exec(' + str + ')');
  return str.length >= pasMinLength && str.length <= pasMaxLength && _test ? true : false;
};

function checkField(str, _type, _empty, str_num) {
  var pattern = '';
  if(_type == 'select') {
    str = parseInt(str);
    pattern = '';
  } else if(_type == 'password') {
    if(_empty && str.length == 0) return 'ok';
    if(!checkPas(str)) return 'error';
  } else if(_type == 'confirm') {
    //console.log(confirmPassword(str));
    if(_empty && str.length == 0 && confirmPassword(str)) return 'ok';
    if(str.length == 0) return 'empty';
    if(!confirmPassword(str)) return 'error';
    pattern = '';
  } else {
    switch(_type) {
      case 'mail':
        pattern = /[a-zA-Zа-яА-Я0-9_-]+@[a-z\.A-Zа-яА-Я0-9_-]+\.[ru|com|info|org|su|net]{2,4}/g;
        break;
      case 'lettersonly':
        pattern = /[a-zA-Zа-яА-Я ]+/g;
        break;
      default:
        pattern = '';
        break;
    }
  }
  if(_type != 'confirm' && _type != 'password' && pattern != '') {
    var _test = pattern.exec(str);
    if(!_test || _test != str) {
      return 'error';
    }
  }
  if(typeof(str) == 'string' && str.length == 0 && !_empty) return 'empty';
  else if(typeof(str) == 'number' && str == 0) return 'error';
  else if(str_num > 0 && str.length != str_num) return 'error';
  else return 'ok';
};

function valid(obj) {
  var _id = obj.attr('id');
  var errorMsg = '';
  var _type = null;
  var _empty = false;
  if(obj.hasClass('name')) _type = 'lettersonly';
  else if(obj.hasClass('phone')) _type = 'phone';
  else if(obj.hasClass('mail')) _type = 'mail';
  else if(obj.hasClass('password')) _type = 'password';
  else if(obj.hasClass('confirm')) _type = 'confirm';
  else if(obj.hasClass('select')) _type = 'select';
  if(obj.hasClass('empty')) _empty = true;
  var str_length = obj.attr("str_length") != 'undefined' && obj.attr("str_length") > 0 ? obj.attr("str_length") : 0;
  var _error = false;
  var checkResult = checkField(obj.val(), _type, _empty, str_length);
  if(checkResult == 'empty' || checkResult == 'error') _error = true;
  if(_error) {
    $('#' + _id).parent('div.form-group').addClass('has-error');
    errorMsg = checkResult == 'empty' ? errMessages.empty : eval('errMessages.error.' + _type);
    if(errorMsg && errorMsg != '') {
      if($('#' + _id).prev('label')) $('#' + _id).prev('label').remove();
      $('#' + _id).before('<label class="control-label" for="' + _id + '">' + errorMsg + '</label>');
    }
  } else {
    $('#' + _id).parent('div.form-group').removeClass('has-error');
    if($('#' + _id).prev('label')) $('#' + _id).prev('label').remove();
  }
  return !_error ? true : false;
};

function formValid(formObj) {
  var allright = true;
  $.each(formObj.find('.' + fieldClass), function(index, item) {
    if(!valid($(item))) allright = false;
  });
  return allright;
};
$(document).ready(function() {
  $('form .' + fieldClass).on('blur', function() {
    valid($(this));
  });
});

