$(document).ready(function() {
  var dialogBox = $('#rolesForm');
  var roleDialog = dialogBox.dialog({
    autoOpen: false,
    show: {
      effect: 'fade',
      duration: 500
    },
    hide: {
      effect: 'fade',
      duration: 500
    }
  });
  var sortableElClass = '.sortable';

  function resultAlert(imgLoad, resultBox, msg) {
    imgLoad.toggle();
    resultBox.css('display', 'inline');
    resultBox.text(msg);
    return setTimeout(function() {
      resultBox.fadeOut('slow');
    }, 500);
  };
  $("#roles-edit").on('click', function() {
    socket.get("/admin/roles.table", function(response) {
      dialogBox.html(JSON.parse(response));
      roleDialog.dialog('open');
      /*** Set draggable object ***/
      if($(sortableElClass)) {
        var imgLoad = dialogBox.children('img.load');
        var resultBox = dialogBox.children('.resultbox');
        sort = dialogBox.find(sortableElClass).sortable({
          axis: 'y',
          scroll: false,
          containment: '.sortbox',
          stop: function(event, ui) {
            imgLoad.toggle();
            sort.sortable('disable');
            dialogBox.addClass('inactive');
            var newRolesImportanceArr = [];
            $.each($(sortableElClass + ' li > div.row'), function() {
              newRolesImportanceArr.push(parseInt($(this).prop('id')));
            });
            socket.request('/admin/roles.importance.save', {
              data: JSON.stringify(newRolesImportanceArr)
            }, function(response) {
              if(response == 'done') {
                resultAlert(imgLoad, resultBox, 'Saved');
                sort.sortable('enable');
                dialogBox.removeClass('inactive');
              }
            });
          },
          handle: 'div.glyphicon'
        });
      }
    });
  });
});

