function rolesPick(selectObj, count) {
  selectObj.selectpicker({
    style: 'btn-info',
    size: count
  });
};

function getList(selectObj, action) {
  var refresh = action == 'refresh' ? true : false;
  var countList = null;
  var rID = $('#rid') ? $('#rid').val() : '';
  socket.get("/admin/roles.select/" + rID, function(response) {
    var response = JSON.parse(response);
    var count = response.count + 2;
    selectObj.children('optgroup').html(response.text);
    if(refresh) {
      selectObj.selectpicker({
        size: count
      });
      selectObj.selectpicker('refresh');
    } else {
      rolesPick(selectObj, count);
    }
  });
};
$(document).ready(function() {
  var selectObj = $('#role.select');
  /* If list object exists in the DOM on page */
  if(selectObj.length > 0) {
    getList(selectObj);
  }
  $('#useradd .form-group').on('click', 'button[data-id="role"]', function() {
    getList(selectObj, 'refresh');
  });
});

