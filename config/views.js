module.exports.views = {
  engine: 'ejs',
  layout: 'layouts/default',
  adminLayout: 'layouts/admin',
  ajaxLayout: 'layouts/ajax'
};

