module.exports.policies = {
  '*': 'sessionAuth',
  SessionController: {
    '*': true
  },
  UserController: {
    'card': ['sessionAuth', 'superAdminMark', 'isUserEditable'],
    'show': ['sessionAuth', 'superAdminMark', 'isUserEditable'],
    'update': ['sessionAuth', 'superAdminMark', 'isUserEditable'],
    'remove': ['sessionAuth', 'superAdminMark', 'isUserEditable'],
    'index': ['sessionAuth', 'superAdminMark']
  },
  RolesController: {
    '*': ['sessionAuth', 'isSuperAdmin'],
    'select': ['sessionAuth'],
    'getRolesList': ['sessionAuth']
  },
  RegisterController: {
    '*': true
  },
  ActionsController: {
    'postJson': true
  },
  StatController: {
    '*': ['sessionAuth']
  },
  LocaleController: {
    '*': ['sessionAuth']
  }
};

