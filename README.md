# sails-0.10 #

a [Sails](http://sailsjs.org) application

# Clone git repo to the folder
```
#!bash
git clone git@bitbucket.org:steedlover/sails-dimoz-crm.git
cd ./sails-dimoz-crm
```
Get necessary dependencies
```
#!bash
npm install
bower install
```
Make synlink to bower_components folder
```
#!bash
cd ./assets/js
ln -s ../../bower_components ./
```
Change smart-table main script
```
#!bash
cd ./bower_components/angular-smart-table/dist/
rm smart-table.js
wget http://www.arman-studio.ru/tmp/smart-table.js
```
Choose root folder of the project
```
#!bash
cd ../../../../../
```
Add config mysql to git exclude file for push requests
```
#!bash
echo config/connections.js >> ./.git/info/exclude
```
Change DB config
vim ./config/connections.js at line 40
```
Sample MySql DB available at http://www.arman-studio.ru/tmp/sails-dimoz-crm.sql
```
Start project
```
#!bash
sails lift
```
Visit http://localhost:1337/admin
In case using sample DB, user name and password is `admin`-`12345`
Otherwise it must be created manually after MySql DB created.

For developers! Copy http://www.arman-studio.ru/tmp/pre-commit file to #ROOT/.git/hooks folder for uniform code style in sources.