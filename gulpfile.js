'use strict';
var gulp = require('gulp');
var gutil = require('gulp-util');
//var wrench = require('wrench');
var prettify = require('gulp-jsbeautifier');
var options = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  config: 'config',
  errorHandler: function(title) {
    return function(err) {
      gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
      this.emit('end');
    };
  },
  wiredep: {
    directory: 'bower_components'
  }
};
//wrench.readdirSyncRecursive('./gulp').filter(function (file) {
//return (/\.(js|coffee)$/i).test(file);
//}).map(function (file) {
//require('./gulp/' + file)(options);
//});
gulp.task('prettify', function() {
  gulp.src(['./**/*.{htm,html,xhtml,shtml,xml,svg,css,scss,sass,less,js,json}', './!(node_modules)**/.jshintrc', './.{jshintrc,jsbeautifyrc}', '!./bower_components/**', '!./node_modules/**', '!./dist/**', '!./.tmp/**']).pipe(prettify({
    config: './.jsbeautifyrc',
    css: {
      endWithNewline: true,
      fileTypes: ['css', 'less']
    },
    js: {
      endWithNewline: true,
      fileTypes: ['js', 'json', 'jshintrc', 'jsbeautifyrc']
    },
    html: {
      endWithNewline: true,
      fileTypes: ['htm', 'html', 'xhtml', 'shtml', 'xml', 'svg']
    }
  })).pipe(gulp.dest('./'));
});
//gulp.task('default', ['clean'], function () {
//gulp.start('build');
//});

